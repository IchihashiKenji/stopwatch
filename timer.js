onload = (function() {
    var startTime,
        stopTime,
        running = false,
        timerId,
        rapCount = 0,
        rapTime;

    document.getElementById('run').onclick = function() {
        run();
    }
    document.getElementById('stop').onclick = function() {
        stop();
    }
    document.getElementById('reset').onclick = function() {
        reset();
    }
    document.getElementById('rap').onclick = function() {
        rap();
    }

    function run() {
        if (running) return;

        running = true;

        if (stopTime) {
            startTime = startTime + (new Date()).getTime() - stopTime;
        }
        if (!startTime) {
            startTime = (new Date()).getTime();
        }
        timer();
    }

    function timer() {
        document.getElementById('sec').innerHTML = (((new Date()).getTime() -
            startTime) / 1000).toFixed(2);
            timerId = setTimeout(function() {
                timer();
            }, 10);
    }

    function stop() {
        if (!running) return false;

        running = false;
        clearTimeout(timerId);
        stopTime = (new Date()).getTime();
    }

    function reset() {
        if (running)return;
        startTime = undefined;
        document.getElementById('sec').innerHTML = '0.00';
        removeElement();
        rapCount = 0;
    }

    function rap() {
        createElement(rapCount);
        rapTime = document.getElementById('sec').innerHTML;
        document.getElementById('rapSec_' + rapCount).innerHTML = rapTime;
        rapCount++;
    }

    function createElement(rapCount) {
        var parentObj = document.getElementById("resCreateElement");
        if (document.getElementById('rapTimeId') == undefined) {
            var olObj = parentObj.appendChild(document.createElement('ol'));
            olObj.setAttribute('id', 'rapTimeId');
        } else {
            var olObj = document.getElementById('rapTimeId')
        }
        var liObj = olObj.appendChild(document.createElement('li'));
        liObj.setAttribute('id','rapSec_' + rapCount);
    }

    function removeElement() {
        var parentObj = document.getElementById("resCreateElement");
        if (document.getElementById('rapTimeId') == undefined) return;
        var childObj = parentObj.childNodes[0];
        parentObj.removeChild(childObj);
    }
});
